import QtQuick 2.0

Rectangle {
    id: field
    width: 600
    height: 400
    border.width: 1
    clip: true
    //Player {}
    signal createBrick(int x, int y)

    Image {
        id: tile
        anchors.fill: parent
        fillMode: Image.Tile
        source: "qrc:/resources/img/ground.png"
    }
    MouseArea {
        anchors.fill: parent
        signal createBlock(int x, int y)
        onClicked: {
            console.log("mouse clicked")
            field.createBrick(mouseX, mouseY)
        }
    }
}
