import QtQuick 2.0

Rectangle {
    id: pauseView
    width: 320
    height: 250
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#dde6a7"
        }

        GradientStop {
            position: 0.364
            color: "#ffffff"
        }

        GradientStop {
            position: 0.779
            color: "#ffffff"
        }

        GradientStop {
            position: 1
            color: "#dde6a7"
        }


    }
    anchors.centerIn: parent
    property string pText: "PAUSE"

    signal restartGame()
    signal quitGame()

    Text {
        id: pauseText
        x: 140
        y: 63
        color: "#424242"
        text: pText
        anchors.horizontalCenter: parent.horizontalCenter
        font.bold: true
        font.pixelSize: 28
    }

    Item {
        x: 100
        width: 160
        height: 32
        anchors.top: pauseText.bottom
        anchors.topMargin: 25
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: textRestart
            y: 164
            text: qsTr("Restart game")
            anchors.leftMargin: 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.left: imageRestart.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            font.bold: true
            font.pixelSize: 15
        }

        Image {
            id: imageRestart
            width: 32
            height: 32
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            source: "qrc:/resources/img/restart.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                pauseView.restartGame()
            }
        }
    }

    Item {
        x: 92
        width: 160
        height: 36
        anchors.top: pauseText.bottom
        anchors.topMargin: 62
        anchors.horizontalCenterOffset: 0
        Text {
            id: textExit
            y: 164
            text: qsTr("Exit")
            anchors.leftMargin: 0
            font.pixelSize: 15
            anchors.left: imageQuit.right
            verticalAlignment: Text.AlignVCenter
            anchors.right: parent.right
            font.bold: true
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
        }

        Image {
            id: imageQuit
            width: 36
            anchors.topMargin: 0
            anchors.bottomMargin: 0
            anchors.bottom: parent.bottom
            source: "qrc:/resources/img/quit.png"
            anchors.top: parent.top
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                pauseView.quitGame()
            }
        }
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
