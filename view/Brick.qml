import QtQuick 2.0

Item {
    width: 20
    height: 20
    Rectangle {
        anchors.fill: parent
        color: "#a05834"
    }
    Image {
        anchors.fill: parent
        source: "qrc:/resources/img/wall.png"
    }
}
