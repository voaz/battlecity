import QtQuick 2.0
import QtMultimedia 5.0

Item {
    property bool isEOL: false
    id: bullet
    width: 10
    height: 10
    transformOrigin: Item.Center
    state: "fly"
    Image {
        id: bulletSprite
        source: "qrc:/resources/img/bullet.png"
        anchors.centerIn: parent
    }
    states: [
        State {
            name: "fly"
            PropertyChanges {
                target: sprite
                visible: false
            }
        },
        State {
           name: "explosion"
           PropertyChanges {
               target: sprite
               running: true
               visible: true
           }
           PropertyChanges {
               target: bulletSprite
               visible: false
           }
        },
        State {
           name: "eol"
           PropertyChanges {
                target: bullet
                visible: false
           }
        }
    ]
    onStateChanged: {
        if (state == "explosion") {
            explosionSound.play()
        }
    }
    Item {
        width: 25
        height: 25
        visible: true
        anchors.centerIn: parent
        AnimatedSprite {
            id: sprite
            visible: false
            running:  false
            anchors.fill: parent
            anchors.centerIn: parent
            source: "qrc:/resources/img/explosion.png"
            frameCount: 8
            frameDuration: 50
            //frameSync: true
            frameWidth: 40
            frameHeight: 40
            loops: 1
            onCurrentFrameChanged: {
                if (currentFrame == 7) {
                    sprite.visible = false;
                    bullet.state = "eol";
                }
            }
        }
    }
    SoundEffect {
        id: explosionSound
        source: "qrc:/resources/sound/Explosion+1.wav"
        volume: 0.05
    }

}
