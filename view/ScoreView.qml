import QtQuick 2.0

Rectangle {
    property alias textTanksLeft: textTanksLeft
    property alias textScore: textScore

    width: 120
    height: 200
    color: "#bebebe"
    border.color: "#00515151"
    border.width: 2
    Text {
        id: textScoreLabel
        color: "#6e2323"
        text: qsTr("SCORE")
        font.underline: false
        font.bold: true
        font.pixelSize: 16
    }

    Text {
        id: textScore
        anchors.top: textScoreLabel.bottom
        color: "#747474"
        text: qsTr("Text")
        font.bold: true
        font.pixelSize: 14
    }

    Text {
        id: tanksLeftLabel
        anchors.top: textScore.bottom
        anchors.topMargin: 40
        color: "#6e2323"
        text: qsTr("TANKS LEFT")
        font.bold: true
        font.pixelSize: 16
    }

    Text {
        id: textTanksLeft
        anchors.top: tanksLeftLabel.bottom
        color: "#747474"
        text: qsTr("00")
        font.pointSize: 14
        font.bold: true
    }

}
