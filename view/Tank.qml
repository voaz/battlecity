import QtQuick 2.0
import QtMultimedia 5.0

Item {
    id: tank
    width: 32
    height: 32
    property string tankSprite: "qrc:/resources/img/greenalpha.png"

    states: [
        State {
            name: "idle"
            PropertyChanges {
                target: sprite
                paused: true
            }
            PropertyChanges {
                target: tank
                visible: true
            }
        },
        State {
            name: "move"
            PropertyChanges {
                target: sprite
                paused: false
            }
        },
        State {
            name: "death"
            PropertyChanges {
                target: spriteExplosion
                running: true
                visible: true
            }
            PropertyChanges {
                target: sprite
                visible: false
            }
        },
        State {
            name: "eol"
            PropertyChanges {
                target: tank
                visible: false

            }
        }
    ]
    Item {
        width: 40
        height: 40
        visible: true
        anchors.centerIn: parent
        rotation: 180
        AnimatedSprite {
            id: sprite
            anchors.fill: parent
            anchors.centerIn: parent
            source: tankSprite
            frameCount: 8
            frameDuration: 50
            //frameSync: true
            frameWidth: 84
            frameHeight: 84
        }
    }
    Item {
        width: 50
        height: 70
        visible: true
        anchors.centerIn: parent
        rotation: 180
        AnimatedSprite {
            id: spriteExplosion
            visible: false
            running: false
            anchors.fill: parent
            anchors.centerIn: parent
            source: "qrc:/resources/img/explosion2.png"
            frameCount: 19
            frameDuration: 100
            loops: 1
            frameWidth: 157
            frameHeight: 229
            onRunningChanged: {
                if (running == false) {
                    spriteExplosion.visible = false;
                    tank.state = "eol";
                }
            }
        }
    }
}
