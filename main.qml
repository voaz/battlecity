import QtQuick 2.7
import QtQuick.Controls 2.0
import QtMultimedia 5.0
import "view"

ApplicationWindow {
    id: root
    visible: true
    width: 720
    height: 400
    title: "TANKS"

    property int tanksLeft: 0
    property int score: 0

    signal keyPressed(int key)
    signal keyReleased(int key)
    Item {
        focus: true
        Keys.onPressed: {
            root.keyPressed(event.key)
        }

        Keys.onReleased: {
            root.keyReleased(event.key)
        }
    }
    ScoreView {
        id: scoreView
        anchors.topMargin: 0
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.top: parent.top
        textTanksLeft.text: tanksLeft
        textScore.text: score
    }
    Audio {
        id: playMusic
        source: "qrc:/resources/sound/main.ogg"
        volume: 0.1
        autoPlay: true
    }
}
