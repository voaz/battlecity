#ifndef TANK_H
#define TANK_H

#include <QObject>
#include <QPointF>

#include "global.h"
#include "gameobject.h"

class QQuickItem;
class QQmlApplicationEngine;

//enum Direction {UP = 0, RIGHT, DOWN, LEFT};

class Tank : public GameObject {
    Q_OBJECT
public:
    Tank(QQuickItem *item, int speed);

    QPoint             getNextPosition(global::Direction direction);
    void                move(global::Direction direction);
    void                newBullet();
    global::Direction   getDirection();
    void                setDirection(global::Direction direction);
    void rotateToDirection(global::Direction direction);
    bool isEOLState();
signals:
    void pushBullet(QPoint position, int direction, QQuickItem *parent);

public slots:

private:
    global::Direction   _direction;
    int         _speed;
    bool        _alive;

};

#endif // TANK_H
