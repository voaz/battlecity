#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQuickItem>
#include <QTimer>

#include "player.h"
#include "tank.h"
#include "gamefield.h"
#include "bullet.h"

#define FIRE_INTERVAL 500

Player::Player(GameField *field, QObject *parent) : QObject(parent) {
    _gameField = field;
    _direction = global::Direction::UP;
    _state = global::PlayerState::IDLE;
    _currentPressedKey = 0;
    _fireTimer = new QTimer();
    _fireTimer->setSingleShot(true);
    _fireTimer->setInterval(FIRE_INTERVAL);
}

Player::~Player() {
    delete _fireTimer;
    if (_playerTank != nullptr) {
        delete _playerTank;
    }
}

void Player::handleKeyPressed(int key) {
    if (_state == global::PlayerState::DEATH) {
        return;
    }
    if (key == Qt::Key_Up ||
        key == Qt::Key_Down ||
        key == Qt::Key_Left ||
        key == Qt::Key_Right)
    {
        _currentPressedKey = key;
        changeMovingDirection(key);
        _state = global::PlayerState::MOVE;
    } else {
        switch (key) {
        case Qt::Key_Space: {
            if (!_fireTimer->isActive()) {
                _playerTank->newBullet();
                _fireTimer->start();
            }
            break;
        }
        case Qt::Key_B:
            break;
        }
    }
}

QPoint Player::spawn() const {
    return _spawn;
}

void Player::setSpawn(const QPoint &spawn) {
    _spawn = spawn;
}

Tank *Player::getTank() {
    return _playerTank;
}

void Player::handleKeyReleased(int key) {
    if (_state == global::PlayerState::DEATH) {
        return;
    }
    if (key == Qt::Key_Space) {
        //_firePressed = false;
    }
    if (key == Qt::Key_B) {

    }
    if (_currentPressedKey == key) {
        _currentPressedKey = 0;
        _state = global::PlayerState::IDLE;
    }
}

void Player::changeMovingDirection(int key) {
    switch (key) {
    case Qt::Key_Up:
        _direction = global::Direction::UP;

        break;
    case Qt::Key_Down:
        _direction = global::Direction::DOWN;

        break;
    case Qt::Key_Left:
        _direction = global::Direction::LEFT;
        break;
    case Qt::Key_Right:
        _direction = global::Direction::RIGHT;
        break;
    }
    _playerTank->setDirection(_direction);
}

global::PlayerState Player::state() const {
    return _state;
}

void Player::setState(const global::PlayerState &state) {
    _state = state;

    switch (state) {
    case global::PlayerState::IDLE:
        _playerTank->setState("idle");
        break;
    case global::PlayerState::MOVE:
        _playerTank->setState("move");
        break;
    case global::PlayerState::DEATH:
        _playerTank->setState("death");
        break;
    }
}

void Player::update() {
    if (_state == global::PlayerState::DEATH) {
        if (_playerTank->isEOLState()) {
            _playerTank->setPosition(_spawn);
            _playerTank->setState("idle");
            _state = global::PlayerState::IDLE;
        } else {
            return;
        }
    } else if (_state == global::PlayerState::MOVE) {
        QList<int> indexList = _gameField->getCollidedWallsIndex(_playerTank,
                                                                 _playerTank->getNextPosition(_direction),
                                                                 _playerTank->getDirection());

        if(indexList.isEmpty() && !_gameField->isOutOfField(_playerTank->getNextPosition(_direction),
                                                            _playerTank))
        {
            _playerTank->setState("move");
            _playerTank->move(_direction);
        } else {
            //cant move this direction
            _playerTank->setState("idle");
            _state = global::PlayerState::IDLE;
        }
    } else if (_state == global::PlayerState::IDLE) {
        //idle animation
        _playerTank->setState("idle");
    }
}

void Player::setTank(Tank *tank) {
    _playerTank = tank;
    _spawn = tank->getPosition();
    _playerTank->getItem()->setProperty("tankSprite", "qrc:/resources/img/bluealpha.png");
}
