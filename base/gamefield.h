#ifndef GAMEFIELD_H
#define GAMEFIELD_H

#include <QObject>
#include <QHash>
#include <QPointF>

#include "global.h"

class QQmlApplicationEngine;
class QQuickItem;
class GameObject;

class GameField : public QObject {
    Q_OBJECT
public:
    explicit GameField(QQmlApplicationEngine *engine, QQuickItem *field, QObject *parent = 0);

    bool collisionEntered(const QPointF pos);
    QList<int> collisionEntered(const QPointF pos, const int width, const int height, global::Direction direction);


    void removeWallAtPoint(const QPointF pos);
    void removeWallByIndexies(QList<int> indexList);
    bool isOutOfField(const QPointF beginPos, int width, int height);
    QList<int> getCollidedWallsIndex(GameObject *object, QPoint pos, global::Direction direction);
    bool isOutOfField(const QPointF beginPos, GameObject *object);
signals:

public slots:
    void createBrick(int x,int y);
private:
    QQuickItem             *_field;
    QQmlApplicationEngine  *_engine;
    QHash<int, QQuickItem*> _walls;

    void loadWalls();
    QQuickItem *createBrick(QPointF pos);
    QQuickItem *createBlock(QPointF pos);
    int getIndexOfWall(QPointF pos);
    void saveLevelToConfig();
    QPair<int, int> convertIndexToXY(int index);
    QList<int> getCollisionWallsIndex(QPointF start, QPointF end);
};

#endif // GAMEFIELD_H
