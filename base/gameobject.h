#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QObject>

class QQuickItem;

class GameObject : public QObject {
    Q_OBJECT
public:
    GameObject(QQuickItem *item, QObject *parent = 0);
    virtual ~GameObject();
    void        setState(const QString &state);
    QString     state();
    int         width();
    int         height();
    void        setRotation(const qreal angle);
    QPoint      getPosition();
    void        setPosition(QPoint pos);
    QQuickItem *getItem() const;

signals:

public slots:

private:
    QQuickItem *_item;
};

#endif // GAMEOBJECT_H
