#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQuickItem>

#include "bullet.h"

Bullet::Bullet(const int speed,
               const int direction,
               QQuickItem *item,
               QQuickItem *parentTank,
               QObject *parent)
    : GameObject(item, parent)
{

    _direction = static_cast<global::Direction>(direction);
    _parentTank = parentTank;
    _alive = true;

    switch (direction) {
    case global::UP:
        _increment = QPoint(0, -speed);
        setRotation(-90);
        setPosition(getPosition() - QPoint(width() / 2, 0));
        break;
    case global::DOWN:
        _increment = QPoint(0, speed);
        setRotation(90);
        setPosition(getPosition() - QPoint(width() / 2, 0));
        break;
    case global::LEFT:
        _increment = QPoint(-speed, 0);
        setRotation(180);
        setPosition(getPosition() - QPoint(0, height() / 2));
        break;
    case global::RIGHT:
        _increment = QPoint(speed, 0);
        setPosition(getPosition() - QPoint(0, height() / 2));
        break;
    }
}

Bullet::~Bullet() {
    //delete _bullet;
}

void Bullet::move() {
    if (state() == "fly") {
        QPoint newPos = getPosition();
        newPos += _increment;
        setPosition(newPos);
    }
}

QPoint Bullet::getNextPosition() {
    return getPosition() + _increment;
}

global::Direction Bullet::getDirection() const {
    return _direction;
}

void Bullet::update() {
    move();
    if (state() == "eol") {
        emit dead(this);
    }
}

bool Bullet::isEndOfLife() {
    return state() == "eol" ? true : false;
}

bool Bullet::isExploded() {
    if (state() == "explosion") {
        return true;
    }
    return false;
}

QQuickItem *Bullet::getTank() const {
    return _parentTank;
}
