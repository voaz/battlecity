#include <QTime>
#include <QQmlComponent>
#include <QQuickWindow>

#include "mainapp.h"
#include "bullet.h"
#include "player.h"
#include "gamefield.h"
#include "tank.h"
#include "simpletankai.h"
#include "trigger.h"
#include "collisionanalyzer.h"

#define FPS 30
#define TANK_SPEED 3
#define BULLET_SPEED 6
#define ENEMYCOUNT 10
#define TOTAL_PLAYER_LIFES 2

MainApp::MainApp(QObject *parent) : QObject(parent) {
    _engine = new QQmlApplicationEngine();
    _engine->load(QUrl(QLatin1String("qrc:/main.qml")));

    QQuickWindow *window = qobject_cast<QQuickWindow*>(_engine->rootObjects().at(0));
    Q_CHECK_PTR(window);
    window->show();
    QQuickItem *root = window->contentItem();

    QQmlComponent component(_engine, "qrc:/view/PauseView.qml");
    Q_ASSERT(component.isReady());
    _restartView = qobject_cast<QQuickItem*>(component.create());
    _restartView->setParentItem(root);
    _restartView->setParent(_engine);
    _restartView->setZ(500);
    connect(_restartView, SIGNAL(restartGame()), this, SLOT(restartGame()));
    connect(_restartView, SIGNAL(quitGame()), this, SLOT(quitGame()));

    _collision = nullptr;
    _trigger = nullptr;
    _player = nullptr;
    _gameField = nullptr;

    _frameEmulator.setInterval(1000 / FPS);
    connect(&_frameEmulator, &QTimer::timeout, this, &MainApp::update);
}

MainApp::~MainApp() {
    freeResources();
    _engine->deleteLater();
}

void MainApp::initialize() {

    QQuickWindow *window = qobject_cast<QQuickWindow*>(_engine->rootObjects().at(0));
    Q_CHECK_PTR(window);
    window->show();
    QQuickItem *root = window->contentItem();

    QQmlComponent component(_engine, "qrc:/view/Field.qml");
    Q_ASSERT(component.isReady());
    QQuickItem* item = qobject_cast<QQuickItem*>(component.create());
    item->setParentItem(root);
    item->setParent(_engine);
    _field = item;
    _gameField = new GameField(_engine, _field);

    _player = new Player(_gameField, root);
    _player->setTank(createTank(QPoint(20, 360)));
    connect(window, SIGNAL(keyPressed(int)), _player, SLOT(handleKeyPressed(int)));
    connect(window, SIGNAL(keyReleased(int)), _player, SLOT(handleKeyReleased(int)));

    SimpleTankAI *ai = new SimpleTankAI(_gameField);
    ai->setTank(createTank(QPoint(280, 200)));
    _ailist.push_back(ai);

    SimpleTankAI *ai2 = new SimpleTankAI(_gameField);
    ai2->setTank(createTank(QPoint(10, 5)));
    _ailist.push_back(ai2);

    SimpleTankAI *ai3 = new SimpleTankAI(_gameField);
    ai3->setTank(createTank(QPoint(560, 20)));
    _ailist.push_back(ai3);

    _trigger = new Trigger(createTrigger(QPoint(280, 360)));
    connect(_trigger, &Trigger::triggered, this, &MainApp::gameOverTriggered);

    _collision = new CollisionAnalyzer();
    _playerLifesLeft = TOTAL_PLAYER_LIFES;
}

void MainApp::restartGame() {
    freeResources();
    initialize();
    _frameEmulator.start();
    _restartView->setVisible(false);
}

void MainApp::quitGame() {
    QCoreApplication::exit();
}

void MainApp::freeResources() {
    if (_collision != nullptr) {
        delete _collision;
    }
    if (_trigger != nullptr) {
        delete _trigger;
    }
    for (auto ai : _ailist) {
        delete ai;
    }
    _ailist.clear();
    if (_player != nullptr) {
        delete _player;
    }
    if (_gameField != nullptr) {
        delete _gameField;
    }
    updateScore(0);
}

void MainApp::update() {
    _player->update();

    QVector<Bullet*>::iterator itbullet = _bullets.begin();
    while (itbullet != _bullets.end()) {
        Bullet *bullet = *itbullet;
        if (!bullet->isEndOfLife()) {
            if (!bullet->isExploded()) { //check collisions with field
                QPoint nextPos = bullet->getNextPosition();
                QList<int> indexList = _gameField->getCollidedWallsIndex(bullet,
                                                                         nextPos,
                                                                         bullet->getDirection());
                if(!indexList.isEmpty() || _gameField->isOutOfField(nextPos, bullet)) {
                    _gameField->removeWallByIndexies(indexList);
                    bullet->setState("explosion");
                }
            }
            if (!bullet->isExploded()) {  //check collisions with trigger
                if (CollisionAnalyzer::detectCollision(bullet, _trigger)) {
                    bullet->setState("explosion");
                    emit _trigger->triggered();
                }
            }
            if (!bullet->isExploded()) {  //check collisions with player tank
                if (_player->getTank()->getItem() != bullet->getTank() &&
                           CollisionAnalyzer::detectCollision(bullet, _player->getTank()))
                {
                    _player->setState(global::PlayerState::DEATH);
                    _playerLifesLeft--;
                    bullet->setState("explosion");
                    if (_playerLifesLeft == 0) {
                        pause("YOU DIED");
                    }
                }
            }
            if (!bullet->isExploded()) {  //check collisions with ai
                for (auto ai : _ailist) {
                    if (ai->getTank()->state().compare("death") == 0) {
                        continue;
                    }
                    if (bullet->getTank() == ai->getTank()->getItem()) {
                        continue;
                    }
                    if (bullet->isExploded()) {
                        continue;
                    }
                    if (CollisionAnalyzer::detectCollision(bullet, ai->getTank())) {
                        if (bullet->getTank() == _player->getTank()->getItem()) {
                            updateScore(_score + 100);
                            if (_score == ENEMYCOUNT * 100) {
                                pause("VICTORY!");
                            }
                        }
                        ai->getTank()->setState("death");
                        bullet->setState("explosion");
                    }
                }
            }
            if (!bullet->isExploded()) {  //check collisions with bullets
                for (auto bulletSecond : _bullets) {
                    if (bullet->getTank() == bulletSecond->getTank()) {
                        continue;
                    }
                    if (bulletSecond->isExploded()) {
                        continue;
                    }
                    if (bullet == bulletSecond) {
                        continue;
                    }
                    if (CollisionAnalyzer::detectCollision(bullet, bulletSecond)) {
                        bullet->setState("explosion");
                        bulletSecond->setState("explosion");
                        break;
                    }
                }
            }
            bullet->update();
            itbullet++;
        } else {
            itbullet = _bullets.erase(itbullet);
            delete bullet;
        }

    }
    for (auto ai : _ailist) {
        if (ai->isDead()) {
            ai->moveToSpawn();
        } else {
            ai->update();
        }
    }
}

void MainApp::pullBullet(const QPoint pos, const int direction, QQuickItem *parent) {
    QQuickItem *item = createBullet(pos);
    Bullet *bullet = new Bullet(BULLET_SPEED, direction, item, parent);
    _bullets.push_back(bullet);
}

void MainApp::gameOverTriggered() {
// pause timer, game over window
    pause(QString("YOU DIED"));
}

void MainApp::pause(const QString message) {
    _frameEmulator.stop();
    _restartView->setProperty("pText", message);
    _restartView->setVisible(true);
}

Tank* MainApp::createTank(QPoint pos) {
    QQmlComponent tankComponent(_engine, "qrc:/view/Tank.qml");
    Q_ASSERT(tankComponent.isReady());
    QQuickItem* tankItem = qobject_cast<QQuickItem*>(tankComponent.create());

    tankItem->setParentItem(_field);
    tankItem->setParent(_engine);
    Tank *tank = new Tank(tankItem, TANK_SPEED);
    tank->setPosition(pos);
    connect(tank, &Tank::pushBullet, this, &MainApp::pullBullet);
    return tank;
}

QQuickItem* MainApp::createTrigger(QPoint pos) {
    QQmlComponent trigger(_engine, "qrc:/view/Trigger.qml");
    Q_ASSERT(trigger.isReady());
    QQuickItem* triggerItem = qobject_cast<QQuickItem*>(trigger.create());

    triggerItem->setParentItem(_field);
    triggerItem->setParent(_engine);
    triggerItem->setPosition(pos);
    return triggerItem;
}

QQuickItem* MainApp::createBullet(QPoint pos) {
    QQmlComponent bullet(_engine, "qrc:/view/Bullet.qml");
    Q_ASSERT(bullet.isReady());
    QQuickItem* bulletItem = qobject_cast<QQuickItem*>(bullet.create());

    bulletItem->setPosition(pos);
    bulletItem->setParentItem(_field);
    bulletItem->setParent(_engine);
    return bulletItem;
}

void MainApp::updateScore(const int score) {
    QQuickWindow *window = qobject_cast<QQuickWindow*>(_engine->rootObjects().at(0));
    _score = score;
    window->setProperty("score", _score);
    window->setProperty("tanksLeft", ENEMYCOUNT - _score / 100);
}
