#ifndef BULLET_H
#define BULLET_H

#include <QObject>
#include <QPointF>

#include "global.h"
#include "gameobject.h"

class QQuickItem;
class QQmlApplicationEngine;

class Bullet : public GameObject {
    Q_OBJECT
public:
    explicit Bullet(const int speed, const int direction, QQuickItem *item,
                    QQuickItem *parentTank,
                    QObject *parent = 0);
    ~Bullet();
    void move();
    QPoint getNextPosition();
    void update();
    bool isEndOfLife();

    global::Direction getDirection() const;

    bool isExploded();
    QQuickItem *getTank() const;


signals:
    void dead(Bullet *bullet);
public slots:
private:
    QPoint             _increment;
    bool                _alive;
    global::Direction   _direction;
    QQuickItem         *_parentTank;
};

#endif // BULLET_H
