#ifndef MAINAPP_H
#define MAINAPP_H

#include <QObject>
#include <QTimer>
#include <QQuickItem>
#include <QVector>
#include <QQmlApplicationEngine>

class Bullet;
class Player;
class GameField;
class Tank;
class SimpleTankAI;
class Trigger;
class CollisionAnalyzer;

class MainApp : public QObject {
    Q_OBJECT
public:
    explicit MainApp(QObject *parent = 0);
    ~MainApp();
signals:

public slots:

private slots:
    void initialize();
    void pullBullet(const QPoint pos, const int direction, QQuickItem *parent);
    void gameOverTriggered();
    void restartGame();
    void quitGame();
    void pause(const QString message);

private:
    QTimer                  _frameEmulator;
    QQuickItem             *_field;
    Player                 *_player;
    QVector<Bullet*>        _bullets;
    QQmlApplicationEngine  *_engine;
    GameField              *_gameField;
    QVector<SimpleTankAI*>  _ailist;
    Trigger                *_trigger;
    CollisionAnalyzer      *_collision;
    QQuickItem             *_restartView;
    int                     _score;
    int                     _playerLifesLeft;
    QQuickItem *createTrigger(QPoint pos);
    QQuickItem *createBullet(QPoint pos);
    Tank *createTank(QPoint pos);
    void deployAI();
    void freeResources();
    void update();

    void updateScore(const int score);
};

#endif // MAINAPP_H
