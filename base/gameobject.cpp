#include <QQuickItem>

#include "gameobject.h"

GameObject::GameObject(QQuickItem *item, QObject *parent) :
    QObject(parent), _item(item)
{
}

GameObject::~GameObject() {
    delete _item;
}

void GameObject::setState(const QString &state) {
    _item->setState(state);
}

QString GameObject::state() {
    return _item->state();
}

QPoint GameObject::getPosition() {
    return _item->position().toPoint();
}

void GameObject::setPosition(QPoint pos) {
    _item->setPosition(pos);
}

QQuickItem *GameObject::getItem() const {
    return _item;
}


int GameObject::width() {
    return _item->width();
}

int GameObject::height() {
    return _item->height();
}

void GameObject::setRotation(const qreal angle) {
    _item->setRotation(angle);
}

