/*****************************************************************************
 * wintrigger.h
 *
 * Created: 6 2017 by amir
 *
 * Copyright 2017 "INTERSET". All rights reserved.
**************************************************************************/
#ifndef WINTRIGGER_H
#define WINTRIGGER_H

#include "gameobject.h"

class Trigger : public GameObject {
    Q_OBJECT
signals:
    void triggered();
public:
    Trigger(QQuickItem *item);
};

#endif // WINTRIGGER_H
