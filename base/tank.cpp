#include <QQmlApplicationEngine>
#include <QQuickItem>

#include "tank.h"

Tank::Tank(QQuickItem *item, int speed) : GameObject(item) {
    _direction = global::Direction::UP;
    _speed = speed;
    _alive = true;
}

void Tank::setDirection(global::Direction direction) {
    _direction = direction;
    rotateToDirection(direction);
}

global::Direction Tank::getDirection() {
    return _direction;
}

QPoint Tank::getNextPosition(global::Direction direction) {
    QPoint pos = getPosition();
    switch (direction) {
    case global::UP:
        pos += QPoint(0, -_speed);
        break;
    case global::DOWN:
        pos += QPoint(0, _speed);
        break;
    case global::LEFT:
        pos += QPoint(-_speed, 0);
        break;
    case global::RIGHT:
        pos += QPoint(_speed, 0);
        break;
    }
    return pos;
}

void Tank::move(global::Direction direction) {
    _direction = direction;
    rotateToDirection(direction);
    setPosition(getNextPosition(direction));
}

void Tank::newBullet() {
    QPoint pos = getPosition();
    switch (_direction) {
    case global::UP:
        pos += QPoint(width() / 2, 0);
        break;
    case global::DOWN:
        pos += QPoint(width() / 2, height());
        break;
    case global::LEFT:
        pos += QPoint(0, height() / 2);
        break;
    case global::RIGHT:
        pos += QPoint(width(), height() / 2);
        break;
    }
    emit pushBullet(pos, _direction, getItem());
}

void Tank::rotateToDirection(global::Direction direction) {

    switch (direction) {
    case global::UP:
        setRotation(0);
        break;
    case global::DOWN:
        setRotation(180);
        break;
    case global::LEFT:
        setRotation(270);
        break;
    case global::RIGHT:
        setRotation(90);
        break;
    }
}

bool Tank::isEOLState() {
    return state().compare("eol") == 0 ? true : false;
}
