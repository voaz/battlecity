#ifndef GLOBAL_H
#define GLOBAL_H

namespace global {
    enum Direction {UP = 0, RIGHT, DOWN, LEFT};
    enum PlayerState {IDLE, MOVE, DEATH};
    enum WallType {BLOCK, BRICK};
    const int WALL_WIDTH = 20;
    const int WALL_HEIGHT = 20;
    const int MOVES_PER_FIRE = 80;
}

#endif // GLOBAL_H
