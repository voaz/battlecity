
#include "simpletankai.h"
#include "tank.h"
#include "gamefield.h"

SimpleTankAI::SimpleTankAI(GameField *field, QObject *parent) : QObject(parent) {
    _field = field;
    _movesCount = 0;
}

void SimpleTankAI::update() {
    if (_tank->state() == "eol") {
        moveToSpawn();
        return;
    }
    if (_tank->state() != "death") {
        if (_movesCount == global::MOVES_PER_FIRE) {
            _tank->newBullet();
            _movesCount = 0;
        }
        _tank->setState("move");
        QPoint nextPos = _tank->getNextPosition(_tank->getDirection());
        QList<int> indexList = _field->getCollidedWallsIndex(_tank, nextPos,
                                                            _tank->getDirection());

        if(indexList.isEmpty() && !_field->isOutOfField(nextPos, _tank)) {
            _tank->move(_tank->getDirection());
            _movesCount++;
        } else {
            _tank->setDirection(getAvailableDirection());
            _tank->move(_tank->getDirection());
            _movesCount++;
        }
    }
}

void SimpleTankAI::setTank(Tank *tank) {
    _tank = tank;
    _spawn = tank->getPosition();
}

Tank *SimpleTankAI::getTank()
{
    return _tank;
}

void SimpleTankAI::setState(global::PlayerState state)
{
    switch (state) {
    case global::PlayerState::IDLE:
        _tank->setState("idle");
        break;
    case global::PlayerState::DEATH:
        _tank->setState("death");
        break;
    default:
        break;
    }
}

bool SimpleTankAI::isDead() {
    return _tank->isEOLState();
}

void SimpleTankAI::moveToSpawn() {
    _tank->setPosition(_spawn);
    _tank->setState("idle");
    _tank->setDirection(global::Direction::DOWN);
}

global::Direction SimpleTankAI::getAvailableDirection() {
    //qrand()
    if (_tank->getDirection() != global::Direction::LEFT) {
        if (isDirectionWithoutCollisions(global::Direction::LEFT)) {
            return global::Direction::LEFT;
        }
    }
    if (_tank->getDirection() != global::Direction::DOWN) {
        if (isDirectionWithoutCollisions(global::Direction::DOWN)) {
            return global::Direction::DOWN;
        }
    }
    if (_tank->getDirection() != global::Direction::RIGHT) {
        if (isDirectionWithoutCollisions(global::Direction::RIGHT)) {
            return global::Direction::RIGHT;
        }
    }
    if (_tank->getDirection() != global::Direction::UP) {
        if (isDirectionWithoutCollisions(global::Direction::UP)) {
            return global::Direction::UP;
        }
    }
    return global::Direction::DOWN;
}

bool SimpleTankAI::isDirectionWithoutCollisions(global::Direction direction) {
    QPoint nextPos = _tank->getNextPosition(direction);
    QList<int> indexList = _field->getCollidedWallsIndex(_tank,nextPos,
                                                        _tank->getDirection());

    if(indexList.isEmpty() && !_field->isOutOfField(nextPos, _tank)) {
        return true;
    }
    return false;
}
