#include <QVector2D>
#include <QQuickItem>
#include <QQmlComponent>
#include <QQmlApplicationEngine>
#include <cmath>
#include <QFile>
#include <QTextStream>

#include "gamefield.h"
#include "gameobject.h"

GameField::GameField(QQmlApplicationEngine *engine, QQuickItem *field, QObject *parent) :
    QObject(parent),
    _field(field),
    _engine(engine)
{
    loadWalls();
    connect(_field, SIGNAL(createBrick(int,int)), this, SLOT(createBrick(int,int)));
}

bool GameField::collisionEntered(const QPointF pos) {
    double xRound = 0;
    modf(pos.x() / global::WALL_WIDTH, &xRound);
    double yRound = 0;
    modf(pos.y() / global::WALL_HEIGHT, &yRound);

    int index = xRound * 1000 + yRound;
    auto it = _walls.value(index);
    if (it != nullptr) {
        return false;
    }
    if (pos.x() < 0 || pos.x() > _field->width() ||
        pos.y() < 0 || pos.y() > _field->height())
    {
        return true;
    }

    return false;
}

QList<int> GameField::collisionEntered(const QPointF pos,
                                       const int width,
                                       const int height,
                                       global::Direction direction) {
    QList<int> walls;
    switch (direction) {
    case global::UP:
        walls = getCollisionWallsIndex(pos, QPointF(pos.x() + width, pos.y()));
        break;
    case global::DOWN:
        walls = getCollisionWallsIndex(QPointF(pos.x(), pos.y() + height),
                                  QPointF(pos.x() + width, pos.y() + height));
        break;
    case global::LEFT: {
        walls = getCollisionWallsIndex(pos, QPointF(pos.x(), pos.y() + height));
        break;
    }
    case global::RIGHT:
        walls = getCollisionWallsIndex(QPointF(pos.x() + width, pos.y()),
                                  QPointF(pos.x() + width, pos.y() + height));
        break;
    }

    return walls;
}

QList<int> GameField::getCollidedWallsIndex(GameObject *object, QPoint pos,
                                       global::Direction direction) {
    QList<int> walls;
    switch (direction) {
    case global::UP:
        walls = getCollisionWallsIndex(pos, QPointF(pos.x() + object->width(), pos.y()));
        break;
    case global::DOWN:
        walls = getCollisionWallsIndex(QPointF(pos.x(), pos.y() + object->height()),
                                  QPointF(pos.x() + object->width(), pos.y() + object->height()));
        break;
    case global::LEFT: {
        walls = getCollisionWallsIndex(pos, QPointF(pos.x(), pos.y() + object->height()));
        break;
    }
    case global::RIGHT:
        walls = getCollisionWallsIndex(QPointF(pos.x() + object->width(), pos.y()),
                                  QPointF(pos.x() + object->width(), pos.y() + object->height()));
        break;
    }

    return walls;
}


bool GameField::isOutOfField(const QPointF beginPos, int width, int height) {
    QPointF endPos(beginPos.x() + width, beginPos.y() + height);
    if (beginPos.x() < 0 || endPos.x() > _field->width() ||
        beginPos.y() < 0 || endPos.y() > _field->height())
    {
        return true;
    }
    return false;
}

bool GameField::isOutOfField(const QPointF beginPos, GameObject *object) {
    QPointF endPos(beginPos.x() + object->width(), beginPos.y() + object->height());

    if (beginPos.x() < 0 || endPos.x() > _field->width() ||
        beginPos.y() < 0 || endPos.y() > _field->height())
    {
        return true;
    }
    return false;
}

QList<int> GameField::getCollisionWallsIndex(QPointF start, QPointF end) {
    QList<int> wallList;

    int indexStart = getIndexOfWall(start);
    QQuickItem *itStart = _walls.value(indexStart);
    if (itStart != nullptr) {
        wallList.push_back(indexStart);
    }
    int indexEnd = getIndexOfWall(end);
    if (indexStart == indexEnd) {
        return wallList;
    }
    QQuickItem *itEnd = _walls.value(indexEnd);
    if (itEnd != nullptr) {
        wallList.push_back(indexEnd);
    }
    QPair<int,int> xyStart = convertIndexToXY(indexStart);
    QPair<int,int> xyEnd = convertIndexToXY(indexEnd);
    if (start.x() - end.x() != 0) {
        size_t count = abs(xyStart.first - xyEnd.first);
        for (size_t btwn = 1; btwn < count; ++btwn) {
            int bIndex = xyStart.first * 1000 + (btwn * 1000) + xyStart.second;
            QQuickItem *itBtwn = _walls.value(bIndex);
            if (itBtwn != nullptr) {
                wallList.push_back(bIndex);
            }
        }
    } else {
        size_t count = abs(xyStart.second - xyEnd.second);
        for (size_t btwn = 1; btwn < count; ++btwn) {
            int bIndex = xyStart.first * 1000 + xyStart.second + btwn;
            QQuickItem *itBtwn = _walls.value(bIndex);
            if (itBtwn != nullptr) {
                wallList.push_back(bIndex);
            }
        }
    }
    return wallList;
}

QPair<int,int> GameField::convertIndexToXY(int index) {
    int y = index % 1000;
    int x = index / 1000;
    return QPair<int,int>(x,y);
}

void GameField::createBrick(int x, int y) {
    double xRound = 0;
    modf(x / global::WALL_WIDTH, &xRound);
    double yRound = 0;
    modf(y / global::WALL_HEIGHT, &yRound);

    int index = xRound * 1000 + yRound;
    if (_walls.find(index) == _walls.end()) {
        QQuickItem *item = createBrick(QPointF(xRound * global::WALL_WIDTH, yRound * global::WALL_HEIGHT));
        _walls.insert(index, item);
        //saveLevelToConfig();
    }
}

void GameField::loadWalls() {
    QFile file("fieldStructure");
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream tStrm(&file);
        while (!tStrm.atEnd()) {
            QString line = tStrm.readLine();
            int indexDelim = line.indexOf(';');
            int x = line.left(indexDelim).toInt();
            int index2 = line.indexOf(';', indexDelim + 1);
            int y = line.mid(indexDelim + 1,
                             index2 - (indexDelim + 1)).toInt();
            global::WallType type = static_cast<global::WallType>(line.right(1).toInt());
            int index = x * 1000 + y;
            QPointF pos(x * global::WALL_WIDTH, y * global::WALL_HEIGHT);
            _walls.insert(index, type == global::WallType::BRICK ?
                              createBrick(pos) : createBlock(pos));
        }
        file.close();
    }
}

void GameField::saveLevelToConfig() {
    QFile file("fieldStructure");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QList<int> keys = _walls.keys();
        QTextStream out(&file);
        for (auto key : keys) {
            QPair<int,int> pair = convertIndexToXY(key);
            out << pair.first << ";" << pair.second << ";1\n";
        }
    }
}

void GameField::removeWallAtPoint(QPointF pos) {
    int index = getIndexOfWall(pos);
    QQuickItem *wall = _walls.value(index);
    if (wall == nullptr) {
        return;
    }
    //if (_wallTypes.value(index) == brick) {}
    _walls.remove(index);
    delete wall;
}

void GameField::removeWallByIndexies(QList<int> indexList) {
    for (auto index : indexList) {
        QQuickItem *wall = _walls.value(index);
        if (wall != nullptr) {
            _walls.remove(index);
            delete wall;
        }
    }
}

int GameField::getIndexOfWall(QPointF pos) {
    double xRound = 0;
    modf(pos.x() / global::WALL_WIDTH, &xRound);
    double yRound = 0;
    modf(pos.y() / global::WALL_HEIGHT, &yRound);

    return xRound * 1000 + yRound;
}

QQuickItem *GameField::createBrick(QPointF pos) {
    Q_CHECK_PTR(_engine);
    QQmlComponent brickComponent(_engine, "qrc:/view/Brick.qml");
    Q_ASSERT(brickComponent.isReady());
    QQuickItem* brick = qobject_cast<QQuickItem*>(brickComponent.create());

    brick->setParentItem(_field);
    brick->setParent(_engine);
    brick->setPosition(pos);
    return brick;
}

QQuickItem *GameField::createBlock(QPointF pos) {
    Q_CHECK_PTR(_engine);
    QQmlComponent blockComponent(_engine, "qrc:/view/Block.qml");
    Q_ASSERT(blockComponent.isReady());
    QQuickItem* block = qobject_cast<QQuickItem*>(blockComponent.create());

    block->setParentItem(_field);
    block->setParent(_engine);
    block->setPosition(pos);
    return block;
}

//QQuickItem *getCollizionObject(const QPointF pos) {}
