#ifndef SIMPLETANKAI_H
#define SIMPLETANKAI_H

#include <QObject>
#include <QPoint>

#include "global.h"

class Tank;
class GameField;

class SimpleTankAI : public QObject {
    Q_OBJECT
public:
    explicit SimpleTankAI(GameField *field, QObject *parent = 0);
    void update();
    void setTank(Tank *tank);
    Tank *getTank();
    void setState(global::PlayerState state);
    bool isDead();
    void moveToSpawn();
signals:

public slots:
private:
    Tank       *_tank;
    GameField  *_field;
    int         _movesCount;
    QPoint      _spawn;
    global::Direction getAvailableDirection();
    bool isDirectionWithoutCollisions(global::Direction direction);
};

#endif // SIMPLETANKAI_H
