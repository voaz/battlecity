#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QPointF>

#include "global.h"

class QQmlApplicationEngine;
class Tank;
class GameField;
class Bullet;
class QQuickItem;
class QTimer;

class Player : public QObject {
    Q_OBJECT
public:

    //explicit Player(QQmlApplicationEngine *engine, QObject *parent = 0);
    explicit Player(GameField *field, QObject *parent = 0);
    ~Player();

    global::PlayerState state() const;
    void setState(const global::PlayerState &state);
    void update();
    void setTank(Tank *tank);

    bool isBulletCollided(Bullet *bullet);
    QPoint spawn() const;
    void setSpawn(const QPoint &spawn);
    Tank *getTank();
signals:

public slots:
    void handleKeyReleased(int key);
    void handleKeyPressed(int key);
private:
    Tank                   *_playerTank;
    GameField              *_gameField;
    global::Direction       _direction;
    global::PlayerState     _state;
    int                     _currentPressedKey;
    QPoint                  _spawn;
    QTimer                 *_fireTimer;

    void changeMovingDirection(int key);
};

#endif // PLAYER_H
