/*****************************************************************************
 * collisionanalyzer.cpp
 *
 * Created: 6 2017 by amir
 *
 * Copyright 2017 "INTERSET". All rights reserved.
**************************************************************************/
#include <QPoint>
#include "collisionanalyzer.h"
#include "gameobject.h"

CollisionAnalyzer::CollisionAnalyzer() {

}

bool CollisionAnalyzer::detectCollision(GameObject *first, GameObject *second) {
    QPoint firstPos = first->getPosition();
    QPoint secondPos = second->getPosition();
    if (firstPos.x() >= secondPos.x() &&
        firstPos.x() <= secondPos.x() + second->width() &&
        firstPos.y() >= secondPos.y() &&
        firstPos.y() <= secondPos.y() + second->height())
    {
        //-_-
        return true;
    } else if (firstPos.x() + first->width() >= secondPos.x() &&
               firstPos.x() + first->width() <= secondPos.x() + second->width() &&
               firstPos.y() + first->height() >= secondPos.y() &&
               firstPos.y() + first->height() <= secondPos.y() + second->height()) {
        return true;
    }
    return false;
}
