/*****************************************************************************
 * collisionanalyzer.h
 *
 * Created: 6 2017 by amir
 *
 * Copyright 2017 "INTERSET". All rights reserved.
**************************************************************************/
#ifndef COLLISIONANALYZER_H
#define COLLISIONANALYZER_H

class GameObject;
class QQuickItem;

class CollisionAnalyzer {
public:
    CollisionAnalyzer();
    static bool detectCollision(GameObject *first, GameObject *second);
};

#endif // COLLISIONANALYZER_H
